package ltoi.domain.mapper;

@FunctionalInterface
public interface ObjectMapper<D,S> {
    D mapTo(S source);
}
