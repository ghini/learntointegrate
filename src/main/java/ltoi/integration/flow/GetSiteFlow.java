package ltoi.integration.flow;

import ltoi.domain.Account;
import ltoi.domain.DataProvider;
import ltoi.domain.mapper.ObjectMapper;
import ltoi.domain.transfer.GetSiteTransferObject;
import ltoi.integration.handler.ServiceRequestHandler;
import ltoi.ns.schema.Site;
import ltoi.ns.service.GetSite;
import ltoi.ns.service.GetSiteResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

import javax.inject.Inject;

@Configuration
public class GetSiteFlow {
    private final ltoi.ns.service.ObjectFactory servFactory = new ltoi.ns.service.ObjectFactory();

    @Inject
    private DataProvider<Account,String> accountProvider;

    @Inject
    private ObjectMapper<Site,Account> accountToSiteMapper;

    @Bean
    public DirectChannel getSiteRequestChannel() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow getSite2Flow() {
        return IntegrationFlows.from(getSiteRequestChannel())
            .transform(GetSite.class, gs -> GetSiteTransferObject.createInstance(gs.getId()))
            .enrich(e ->
                    e.requestChannel("enrichWithAccountChannel")
                        .requestPayloadExpression("payload.id")
                        .propertyExpression("account", "payload")
            )
            .transform(GetSiteTransferObject.class, GetSiteTransferObject::getAccount)
            .transform(accountToSiteMapper)
            .transform(Site.class,s->servFactory.createGetSiteResponse().withSite(s))
            .get();
    }

    @Bean
    public IntegrationFlow enrichWithAccount() {
        return IntegrationFlows.from("enrichWithAccountChannel")
            .handle(accountProvider)
            .get();
    }
}
