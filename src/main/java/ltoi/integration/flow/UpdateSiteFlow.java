package ltoi.integration.flow;

import ltoi.integration.handler.ServiceRequestHandler;
import ltoi.ns.service.UpdateSite;
import ltoi.ns.service.UpdateSiteResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

import javax.inject.Inject;

@Configuration
public class UpdateSiteFlow {
    @Inject
    private ServiceRequestHandler<UpdateSite,UpdateSiteResponse> srHandler;

    @Bean
    public DirectChannel updateSiteRequestChannel() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow updateSiteFlow() {
        return IntegrationFlows.from(updateSiteRequestChannel())
            .handle(srHandler)
            .get();
    }
}
