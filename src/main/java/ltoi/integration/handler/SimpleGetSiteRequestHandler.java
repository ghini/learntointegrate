package ltoi.integration.handler;

import ltoi.ns.service.GetSite;
import ltoi.ns.service.GetSiteResponse;

import javax.inject.Named;

@Named
public final class SimpleGetSiteRequestHandler implements ServiceRequestHandler<GetSite,GetSiteResponse> {
    @Override
    public GetSiteResponse handle(final GetSite parameters) {
        return
            servFactory.createGetSiteResponse().withSite(
                oFactory.createSite()
                .withId("123456789")
                .withName("Test Nem")
                .withPlatform("TEST")
                .withContacts(
                    oFactory.createContact()
                        .withLabel("Primary")
                        .withFirstName("First Name")
                        .withLastName("Last Name")
                        .withEmail("test@test.com")
                        .withPhone("8888888888")
                )
                .withAddresses(
                    oFactory.createAddress()
                        .withLabel("Primary")
                        .withAddressLine1("123 Fake Rd")
                        .withCity("FakeCity")
                        .withStateOrProvince("CA")
                        .withPostalCode("12345")
                        .withCountry("US")
                )
            );
    }
}
