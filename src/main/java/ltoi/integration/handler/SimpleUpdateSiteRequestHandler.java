package ltoi.integration.handler;

import ltoi.ns.service.UpdateSite;
import ltoi.ns.service.UpdateSiteResponse;

import javax.inject.Named;

@Named
public final class SimpleUpdateSiteRequestHandler implements ServiceRequestHandler<UpdateSite,UpdateSiteResponse> {
    @Override
    public UpdateSiteResponse handle(final UpdateSite parameters) {
        return
            servFactory.createUpdateSiteResponse().withSite(
                oFactory.createSite()
                .withId("123456789")
                    .withName("Test Nem")
                    .withPlatform("TEST")
                .withContacts(
                    oFactory.createContact()
                        .withLabel("Primary")
                        .withFirstName("First Name")
                        .withLastName("Last Name")
                        .withEmail("test@test.com")
                        .withPhone("8888888888")
                )
                .withAddresses(
                    oFactory.createAddress()
                        .withLabel("Primary")
                        .withAddressLine1("123 Fake Rd")
                        .withCity("FakeCity")
                        .withStateOrProvince("CA")
                        .withPostalCode("12345")
                        .withCountry("US")
                )
            );
    }
}
