
package ltoi.ns.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ltoi.ns.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSite_QNAME = new QName("http://ns.ltoi/service", "getSite");
    private final static QName _GetSiteResponse_QNAME = new QName("http://ns.ltoi/service", "getSiteResponse");
    private final static QName _UpdateSite_QNAME = new QName("http://ns.ltoi/service", "updateSite");
    private final static QName _UpdateSiteResponse_QNAME = new QName("http://ns.ltoi/service", "updateSiteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ltoi.ns.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSite }
     * 
     */
    public GetSite createGetSite() {
        return new GetSite();
    }

    /**
     * Create an instance of {@link GetSiteResponse }
     * 
     */
    public GetSiteResponse createGetSiteResponse() {
        return new GetSiteResponse();
    }

    /**
     * Create an instance of {@link UpdateSite }
     * 
     */
    public UpdateSite createUpdateSite() {
        return new UpdateSite();
    }

    /**
     * Create an instance of {@link UpdateSiteResponse }
     * 
     */
    public UpdateSiteResponse createUpdateSiteResponse() {
        return new UpdateSiteResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSite }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ns.ltoi/service", name = "getSite")
    public JAXBElement<GetSite> createGetSite(GetSite value) {
        return new JAXBElement<GetSite>(_GetSite_QNAME, GetSite.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSiteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ns.ltoi/service", name = "getSiteResponse")
    public JAXBElement<GetSiteResponse> createGetSiteResponse(GetSiteResponse value) {
        return new JAXBElement<GetSiteResponse>(_GetSiteResponse_QNAME, GetSiteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSite }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ns.ltoi/service", name = "updateSite")
    public JAXBElement<UpdateSite> createUpdateSite(UpdateSite value) {
        return new JAXBElement<UpdateSite>(_UpdateSite_QNAME, UpdateSite.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSiteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ns.ltoi/service", name = "updateSiteResponse")
    public JAXBElement<UpdateSiteResponse> createUpdateSiteResponse(UpdateSiteResponse value) {
        return new JAXBElement<UpdateSiteResponse>(_UpdateSiteResponse_QNAME, UpdateSiteResponse.class, null, value);
    }

}
