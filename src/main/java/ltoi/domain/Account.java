package ltoi.domain;

public final class Account {
    private final String id;
    private final String businessName;
    private final String firstName;
    private final String lastName;
    private final String address;
    private final String city;
    private final String state;
    private final String zip;
    private final String phone;
    private final String email;

    private Account(String id, String businessName, String firstName, String lastName, String address, String city, String state, String zip, String phone, String email) {
        this.id = id;
        this.businessName = businessName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone = phone;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public static class AccountBuilder {
        private String id;
        private String businessName;
        private String firstName;
        private String lastName;
        private String address;
        private String city;
        private String state;
        private String zip;
        private String phone;
        private String email;

        private AccountBuilder() {
        }

        public static AccountBuilder anAccount() {
            return new AccountBuilder();
        }

        public AccountBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public AccountBuilder withBusinessName(String businessName) {
            this.businessName = businessName;
            return this;
        }

        public AccountBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public AccountBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public AccountBuilder withAddress(String address) {
            this.address = address;
            return this;
        }

        public AccountBuilder withCity(String city) {
            this.city = city;
            return this;
        }

        public AccountBuilder withState(String state) {
            this.state = state;
            return this;
        }

        public AccountBuilder withZip(String zip) {
            this.zip = zip;
            return this;
        }

        public AccountBuilder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public AccountBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public AccountBuilder but() {
            return anAccount().withId(id).withBusinessName(businessName).withFirstName(firstName).withLastName(lastName).withAddress(address).withCity(city).withState(state).withZip(zip).withPhone(phone).withEmail(email);
        }

        public Account build() {
            return new Account(id, businessName, firstName, lastName, address, city, state, zip, phone, email);
        }
    }
}
