package ltoi.domain.transfer;

import ltoi.domain.Account;

public final class GetSiteTransferObject {
    private final String id;
    private Account account;

    private GetSiteTransferObject(final String id) {
        this.id = id;
    }

    public static GetSiteTransferObject createInstance(final String id) {
        return new GetSiteTransferObject(id);
    }

    public String getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(final Account account) {
        this.account = account;
    }
}
