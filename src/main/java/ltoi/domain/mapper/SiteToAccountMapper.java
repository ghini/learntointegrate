package ltoi.domain.mapper;

import ltoi.domain.Account;
import ltoi.domain.Account.AccountBuilder;
import ltoi.ns.schema.*;
import javax.inject.Named;

@Named("siteToAccountMapper")
public class SiteToAccountMapper implements ObjectMapper<Account,Site> {
    private final ObjectFactory oFactory = new ObjectFactory();
    @Override
    public Account mapTo(final Site site) {
        final Address address = site.getAddresses().stream().findFirst().orElseGet(oFactory::createAddress);
        final Contact contact = site.getContacts().stream().findFirst().orElseGet(oFactory::createContact);

        return AccountBuilder.anAccount()
            .withId(site.getId())
            .withBusinessName(site.getName())
            .withAddress(address.getAddressLine1())
            .withCity(address.getCity())
            .withState(address.getStateOrProvince())
            .withZip(address.getPostalCode())
            .withFirstName(contact.getFirstName())
            .withLastName(contact.getLastName())
            .withEmail(contact.getEmail())
            .withPhone(contact.getPhone())
            .build();
    }
}
