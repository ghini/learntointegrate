package ltoi.integration.transform;

import ltoi.domain.Account;
import ltoi.domain.Account.AccountBuilder;
import ltoi.domain.DataProvider;
import org.springframework.integration.annotation.Transformer;

import javax.inject.Named;

@Named
public class AccountDataProvider implements DataProvider<Account,String> {
    @Override
    public Account getSingle(final String key) {
        return AccountBuilder.anAccount()
            .withId("10001")
            .withBusinessName("Test Site")
            .withAddress("123 Fake Rd")
            .withCity("Fake city")
            .withState("CA")
            .withZip("12345")
            .withFirstName("First")
            .withLastName("Last")
            .withEmail("test@test.com")
            .withPhone("8888888888")
            .build();
    }
}
