package ltoi.integration.handler;

@FunctionalInterface
public interface ServiceRequestHandler<I,O> {
    ltoi.ns.schema.ObjectFactory oFactory = new ltoi.ns.schema.ObjectFactory();
    ltoi.ns.service.ObjectFactory servFactory = new ltoi.ns.service.ObjectFactory();
    O handle(I input);
}
