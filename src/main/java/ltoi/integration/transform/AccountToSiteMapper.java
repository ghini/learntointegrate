package ltoi.integration.transform;

import ltoi.domain.Account;
import ltoi.domain.mapper.ObjectMapper;
import ltoi.ns.schema.ObjectFactory;
import ltoi.ns.schema.Site;
import org.springframework.integration.annotation.Transformer;

//import org.springframework.integration.annotation.Transformer;


import javax.inject.Named;

@Named
public class AccountToSiteMapper implements ObjectMapper<Site,Account> {
    private final ObjectFactory oFactory = new ObjectFactory();

    @Transformer
    @Override
    public Site mapTo(final Account account) {
        return oFactory.createSite()
            .withId(account.getId())
            .withName(account.getBusinessName())
            .withPlatform("TEST")
            .withAddresses(oFactory.createAddress()
                .withLabel("Primary")
                .withAddressLine1(account.getAddress())
                .withCity(account.getCity())
                .withStateOrProvince(account.getState())
                .withPostalCode(account.getZip())
                .withCountry("US")
            )
            .withContacts(oFactory.createContact()
                .withLabel("Primary")
                .withFirstName(account.getFirstName())
                .withLastName(account.getLastName())
                .withPhone(account.getPhone())
                .withEmail(account.getEmail())
            );
    }
}
