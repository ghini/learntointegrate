package ltoi.domain;

public interface DataProvider<T,K> {
    T getSingle(K key);
}
